package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();
        HashMap<Integer, Integer> answer = new HashMap<>();
        answer.put(startIndex, 0);
        arrayDeque.offerFirst(startIndex);
        while (!arrayDeque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[startIndex][i] > 0) {
                    if (answer.containsKey(i)) {
                        if (answer.get(i) > answer.get(startIndex) + adjacencyMatrix[startIndex][i]) {
                            answer.replace(i, answer.get(startIndex) + adjacencyMatrix[startIndex][i]);
                            if (!arrayDeque.contains(i)) {
                                arrayDeque.offerFirst(i);
                            }
                        }
                    } else {
                        answer.put(i, answer.get(startIndex) + adjacencyMatrix[startIndex][i]);
                        if (!arrayDeque.contains(i)) {
                            arrayDeque.offerFirst(i);
                        }
                    }
                }
            }
            if (!arrayDeque.isEmpty()) {
                startIndex = arrayDeque.pollLast();
            }
        }
        return answer;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int[] row;
        int answer = 0;
        int value;
        int IndexStart = 0;
        Set<Integer> dots = new HashSet<>();
        dots.add(IndexStart);
        while(dots.size() != adjacencyMatrix.length)
        {
            int u = 0;
            int v = 0;
            int minWeight = Integer.MAX_VALUE;
            for(int i : dots)
            {
                row = adjacencyMatrix[i];
                for(int j = 0; j < row.length; j++)
                {
                    value = row[j];
                    if((value < minWeight) && (value != 0) && (!dots.contains(j)))
                    {
                        u = i;
                        v = j;
                        minWeight = value;
                    }
                }

            }
            dots.add(v);
            dots.add(u);
            answer += minWeight;
        }
        return answer;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
